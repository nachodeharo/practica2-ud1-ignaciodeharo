package com.ignaciodeharo.clientesgym.base;

import java.time.LocalDate;

/**
 * Clase Cliente que hereda de Persona, añadiendo como variable de clase el número de cuenta del cliente
 */
public class Cliente extends Persona{
    private Long nCuenta;

    /**
     * Constructor de la clase Cliente que permite inicializar todas las variables de clase.
     * @param nombre nombre del cliente
     * @param apellidos apellidos del cliente
     * @param dni dni del cliente
     * @param fechaAlta fecha de alta del cliente
     * @param nCuenta número de cuenta del cliente
     */
    public Cliente(String nombre, String apellidos, String dni, LocalDate fechaAlta, Long nCuenta) {
        super(nombre, apellidos, dni, fechaAlta);
        this.nCuenta = nCuenta;
    }

    /**
     * Constructor vacío de la clase cliente
     */
    public Cliente(){
        super();
    }

    /**
     * Getter de la variable número de cuente que permite devolver el contenido de la variable de nCuenta
     * @return Long el número de cuenta del cliente
     */
    public Long getnCuenta() {
        return nCuenta;
    }

    /**
     * Setter de la variable nCuenta que permite establecer el valor de la variable.
     * @param nCuenta el número de cuenta del cliente
     */
    public void setnCuenta(Long nCuenta) {
        this.nCuenta = nCuenta;
    }

    /**
     * Método toString que permite la visualización de las variables de la clase Padre y de la misma.
     * @return String de las variable de la clase
     */
    @Override
    public String toString() {
        return "Cliente: |" + getNombre() + "| " + getApellidos() + "| " + getDni() + "| " + getFechaAlta() + "| " + getnCuenta()+"|";
    }
}
