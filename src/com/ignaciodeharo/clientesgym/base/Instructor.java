package com.ignaciodeharo.clientesgym.base;

import java.time.LocalDate;

/**
 * Clase Instructor que hereda de Persona, y tiene el codigo de Activación que será requerrido para poder ser registrado.
 */
public class Instructor extends Persona{
    private String codigoActivacion;


    /**
     * Constructor de la clase Instructor que inicializa todas las variables de la clase Instructor y Persona
     * @param nombre nombre del instructor
     * @param apellidos apellidos del instructor
     * @param dni dni del instructor
     * @param fechaAlta fecha del alta del instructor
     * @param codigoActivacion el código de verificación/activación del instructor
     */
    public Instructor(String nombre, String apellidos, String dni, LocalDate fechaAlta, String codigoActivacion) {
        super(nombre, apellidos, dni, fechaAlta);
        this.codigoActivacion = codigoActivacion;
    }

    /**
     * Constructor vacío de la clase instructor
     */
    public Instructor(){
        super();
    }

    /**
     * Getter de código de activación que devuelve el código de activación
     * @return el código de activación
     */
    public String getCodigoActivacion() {
        return codigoActivacion;
    }

    /**
     * Setter del codigo de activación que permite establecer el código de activación
     * @param codigoActivacion el código de activación
     */
    public void setCodigoActivacion(String codigoActivacion) {
        this.codigoActivacion = codigoActivacion;
    }

    /**
     * Clase toString sobreescrita que permite la visualización de todos los valores de las clases.
     * @return String de los valores de las variables de clase Persona e Instructor.
     */
    @Override
    public String toString() {
        return "Instructor: |" + getNombre() + "| " + getApellidos() + "| " + getDni() + "| " + getFechaAlta() + "| (" + getCodigoActivacion()+")";
    }
}
