package com.ignaciodeharo.clientesgym.base;

import java.time.LocalDate;

/**
 * Clase padre Persona, que permite almacenar el nombre, apellidos, dni y el alta de la fecha de cada persona
 */
public class Persona {
    private String nombre;
    private String apellidos;
    private String dni;
    private LocalDate fechaAlta;

    /**
     * Constructor de la clase Persona que crea una persona introducida por parámetros
     * @param nombre nombre de la persona
     * @param apellidos apellidos de la persona
     * @param dni dni de la persona
     * @param fechaAlta fecha del alta de la persona
     */
    public Persona(String nombre, String apellidos, String dni, LocalDate fechaAlta) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.dni = dni;
        this.fechaAlta = fechaAlta;
    }

    /**
     * Constructor vacío de la clase Persona
     */
    public Persona(){

    }

    /**
     * Getter de la clase Persona que permite mostrar el nombre
     * @return nombre del cliente
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Setter de la clase Persona que permite establecer el nombre del cliente por parámetro
     * @param nombre nombre del cliente
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    /**
     * Getter de la clase Persona que permite mostrar los apellidos
     * @return apellidos del cliente
     */
    public String getApellidos() {
        return apellidos;
    }

    /**
     * Setter de la clase Persona que permite establecer los apellidos del cliente por parámetro
     * @param apellidos apellidos del cliente
     */
    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }
    /**
     * Getter de la clase Persona que permite mostrar el dni
     * @return dni del cliente
     */
    public String getDni() {
        return dni;
    }

    /**
     * Setter de la clase Persona que permite establecer el dni del cliente por parámetro
     * @param dni dni del cliente
     */
    public void setDni(String dni) {
        this.dni = dni;
    }

    /**
     * Getter de la clase Persona que permite mostrar la fecha del alta
     * @return fecha de alta del cliente
     */
    public LocalDate getFechaAlta() {
        return fechaAlta;
    }

    /**
     * Setter de la clase Persona que permite establecer el alta del cliente por parámetro
     * @param fechaAlta fecha de alta del cliente
     */
    public void setFechaAlta(LocalDate fechaAlta) {
        this.fechaAlta = fechaAlta;
    }
}
