package com.ignaciodeharo.clientesgym.util;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.File;

/**
 * Clase Util que permite acceder a distintos métodos para ser utilizados con mayor fluidez
 */
public class Util {
    /**
     * Crea un mensaje de error introduciendo el texto por parámetro
     * @param mensaje el texto del error
     */
    public static void msgError(String mensaje) {
        JOptionPane.showMessageDialog(null,mensaje,"ERROR OCURRIDO",JOptionPane.ERROR_MESSAGE);
    }

    /**
     * Crea un mensaje de confirmación (SI/NO) introduciendo el mensaje y titulo por parámetro
     * @param mensaje el mensaje a mostrar
     * @param titulo el titulo de la ventana
     * @return
     */
    public static int msgConfirmacion(String mensaje, String titulo) {
        return JOptionPane.showConfirmDialog(null,mensaje,titulo,JOptionPane.YES_NO_OPTION);
    }
    public static JFileChooser crearSelectorFicheros(File rutaDefecto, String tipoArchivos, String extension) {
        JFileChooser selectorFichero = new JFileChooser();
        if (rutaDefecto!=null) {
            selectorFichero.setCurrentDirectory(rutaDefecto);
        }
        if (extension!=null) {
            FileNameExtensionFilter filtro = new FileNameExtensionFilter(tipoArchivos,extension);
            selectorFichero.setFileFilter(filtro);
        }
        return selectorFichero;
    }
}
