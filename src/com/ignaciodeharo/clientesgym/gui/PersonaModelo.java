package com.ignaciodeharo.clientesgym.gui;

import com.ignaciodeharo.clientesgym.base.Cliente;
import com.ignaciodeharo.clientesgym.base.Instructor;
import com.ignaciodeharo.clientesgym.base.Persona;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Clase PersonaModelo que permite el correcto desarrollo del patron MVC, donde se darán altas, importaciones y exportaciones.
 */
public class PersonaModelo {
    private ArrayList<Persona> listaPersonas;

    /**
     * Constructor de la clase PersonaModelo que permite la inicialización del ArrayList listaPersonas
     */
    public PersonaModelo() {
        listaPersonas = new ArrayList<Persona>();
    }

    /**
     * Getter del ArrayList listaPersonas que permite obtener la lista de personas
     * @return lista de personas (clientes e instructores)
     */
    public ArrayList<Persona> obtenerPersonass() {
        return listaPersonas;
    }

    /**
     * Método que permite dar de alta un Cliente en el Array
     * @param nombre nombre del cliente
     * @param apellidos apellidos del cliente
     * @param dni dni del cliente
     * @param fechaAlta fecha de alta del cliente
     * @param nCuenta número de cuenta del cliente
     */
    public void altaCliente(String nombre, String apellidos, String dni, LocalDate fechaAlta, Long nCuenta) {
        Cliente personita=new Cliente(nombre,apellidos, dni,fechaAlta,nCuenta);
        listaPersonas.add(personita);
    }

    /**
     * Método que permite dar de alta un Instructor en el Array
     * @param nombre nombre del Instructor
     * @param apellidos apellidos del Instructor
     * @param dni dni del Instructor
     * @param fechaAlta fecha de alta del Instructor
     * @param codigo código de verificación del Instructor
     */
    public void altaInstructor(String nombre, String apellidos, String dni, LocalDate fechaAlta, String codigo) {
        Instructor instructor=new Instructor(nombre,apellidos, dni,fechaAlta,codigo);
        listaPersonas.add(instructor);
    }

    /**
     * Metodo que te devuelve un booleano de si existe o no, el dni dentro del array.
     * @param dni el dni de la persona
     * @return boolean si existe o no el dni en el arrayList
     */
    public boolean existeDNI(String dni) {
        for (Persona personita:listaPersonas) {
            if(personita.getDni().equals(dni)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Metodo que permite la exportacion a XML del arrayList
     * @param fichero el ficho al que exportar
     * @throws ParserConfigurationException excepción de conversión
     * @throws TransformerException excepción de conversión
     */
    public void exportarXML(File fichero) throws ParserConfigurationException, TransformerException {

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        DOMImplementation dom = builder.getDOMImplementation();
        Document documento = dom.createDocument(null, "xml", null);

        //Añado el nodo raiz donde la primera etiqueta contendrá las demás
        Element raiz = documento.createElement("Personas");
        documento.getDocumentElement().appendChild(raiz);

        Element nodoPersona = null, nodoDatos = null;
        Text texto = null;

        for (Persona personita : listaPersonas) {

            /*
            Empiezo a añadir las etiquetas dependiendo si es Cliente o Instructor
             */
            if (personita instanceof Cliente) {
                nodoPersona = documento.createElement("Cliente");

            } else {
                nodoPersona = documento.createElement("Instructor");
            }
            raiz.appendChild(nodoPersona);

            /*Dentro de la etiqueta correspondiente le empiezo a añadir los datos generales de las clases
             */
            nodoDatos = documento.createElement("nombre");
            nodoPersona.appendChild(nodoDatos);

            texto = documento.createTextNode(personita.getNombre());
            nodoDatos.appendChild(texto);

            nodoDatos = documento.createElement("apellidos");
            nodoPersona.appendChild(nodoDatos);

            texto = documento.createTextNode(personita.getApellidos());
            nodoDatos.appendChild(texto);

            nodoDatos = documento.createElement("dni");
            nodoPersona.appendChild(nodoDatos);

            texto = documento.createTextNode(personita.getDni());
            nodoDatos.appendChild(texto);

            nodoDatos = documento.createElement("fecha-matriculacion");
            nodoPersona.appendChild(nodoDatos);

            texto = documento.createTextNode(personita.getFechaAlta().toString());
            nodoDatos.appendChild(texto);

            /* Como hay un dato que depende del tipo de persona
            debo acceder a él controlando el tipo de objeto
             */
            if (personita instanceof Cliente) {
                nodoDatos = documento.createElement("Ncuenta");
                nodoPersona.appendChild(nodoDatos);
                texto = documento.createTextNode(String.valueOf(((Cliente) personita).getnCuenta()));
                nodoDatos.appendChild(texto);
            } else {
                nodoDatos = documento.createElement("codigo_Activacion");
                nodoPersona.appendChild(nodoDatos);
                texto = documento.createTextNode(String.valueOf(((Instructor) personita).getCodigoActivacion()));
                nodoDatos.appendChild(texto);
            }

        }
        /*
        Guardo los datos en "fichero" que es el objeto File
        recibido por parametro
         */
        Source source = new DOMSource(documento);
        Result resultado = new StreamResult(fichero);

        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.transform(source, resultado);
    }


    /**
     * Metodo que permite la importación de un XML hacía arrayList
     * @param fichero el ficho importado
     * @throws ParserConfigurationException excepción de conversión
     * @throws TransformerException excepción de conversión
     */
    public void importarXML(File fichero) throws ParserConfigurationException, IOException, SAXException {
        listaPersonas = new ArrayList<Persona>();
        Cliente nuevoCliente = null;
        Instructor nuevoInstructor = null;

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document documento = builder.parse(fichero);

        NodeList listaElementos = documento.getElementsByTagName("*");

        for (int i = 0; i < listaElementos.getLength(); i++) {
            Element nodoPersona = (Element) listaElementos.item(i);


            if (nodoPersona.getTagName().equals("Cliente")) {
                nuevoCliente = new Cliente();
                /*
                nuevoCliente.setNombre(nodoPersona.getChildNodes().item(0).getTextContent());
                nuevoCliente.setApellidos(nodoPersona.getChildNodes().item(1).getTextContent());
                nuevoCliente.setDni(nodoPersona.getChildNodes().item(2).getTextContent());
                System.out.println("-->" + LocalDate.parse(nodoPersona.getChildNodes().item(3).getTextContent()));
                nuevoCliente.setFechaAlta(LocalDate.parse(nodoPersona.getChildNodes().item(3).getTextContent()));
                nuevoCliente.setnCuenta(Long.parseLong(nodoPersona.getChildNodes().item(4).getTextContent()));
                 */

                nuevoCliente.setNombre(nodoPersona.getElementsByTagName("nombre").item(0).getChildNodes().item(0).getNodeValue());
                nuevoCliente.setApellidos(nodoPersona.getElementsByTagName("apellidos").item(0).getChildNodes().item(0).getNodeValue());
                nuevoCliente.setDni(nodoPersona.getElementsByTagName("dni").item(0).getChildNodes().item(0).getNodeValue());
                nuevoCliente.setFechaAlta(LocalDate.parse(nodoPersona.getElementsByTagName("fecha-matriculacion").item(0).getChildNodes().item(0).getNodeValue()));
                nuevoCliente.setnCuenta(Long.parseLong(nodoPersona.getElementsByTagName("Ncuenta").item(0).getChildNodes().item(0).getNodeValue()));
                listaPersonas.add(nuevoCliente);
            } else {
                if (nodoPersona.getTagName().equals("Instructor")) {
                    nuevoInstructor = new Instructor();
                    /*
                    nuevoInstructor.setNombre(nodoPersona.getChildNodes().item(0).getTextContent());
                    nuevoInstructor.setApellidos(nodoPersona.getChildNodes().item(1).getTextContent());
                    nuevoInstructor.setDni(nodoPersona.getChildNodes().item(2).getTextContent());
                    nuevoInstructor.setFechaAlta(LocalDate.parse(nodoPersona.getChildNodes().item(3).getTextContent()));
                    nuevoInstructor.setCodigoActivacion(nodoPersona.getChildNodes().item(4).getTextContent());
                     */
                    nuevoInstructor.setNombre(nodoPersona.getElementsByTagName("nombre").item(0).getChildNodes().item(0).getNodeValue());
                    nuevoInstructor.setApellidos(nodoPersona.getElementsByTagName("apellidos").item(0).getChildNodes().item(0).getNodeValue());
                    nuevoInstructor.setDni(nodoPersona.getElementsByTagName("dni").item(0).getChildNodes().item(0).getNodeValue());
                    nuevoInstructor.setFechaAlta(LocalDate.parse(nodoPersona.getElementsByTagName("fecha-matriculacion").item(0).getChildNodes().item(0).getNodeValue()));
                    nuevoInstructor.setCodigoActivacion(nodoPersona.getElementsByTagName("codigo_Activacion").item(0).getChildNodes().item(0).getNodeValue());
                    listaPersonas.add(nuevoInstructor);
                }
            }


        }
    }

}
