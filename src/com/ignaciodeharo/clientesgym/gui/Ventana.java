package com.ignaciodeharo.clientesgym.gui;

import com.github.lgooddatepicker.components.DatePicker;
import com.ignaciodeharo.clientesgym.base.Persona;

import javax.swing.*;

/**
 * Clase Ventana donde se construye toda la interfaz (junto al .form) y se inicializa lo requerrido
 */
public class Ventana {
    protected JFrame frame;
    private JPanel panel1;
    protected JRadioButton clienteRadioButton;
    protected JRadioButton instructorRadioButton;
    protected JButton registrarButton;
    protected JButton exportarButton;
    protected JButton importarButton;
    protected JButton vaciarTablaButton;
    protected JTextField txtNombre;
    protected JTextField txtApellidos;
    protected JTextField txtDni;
    protected JList list1;
    protected JTextField txtLicenciaCuenta;
    protected JLabel txtLicenciaCod;
    protected DatePicker datePicker;

    //Añado yo
    protected DefaultListModel<Persona> dlmPersona;

    /**
     * Constructor de la clase Ventana que permite la correcta utilización del frame y que llama al método que inicializa la lista
     */
    public Ventana() {
        frame = new JFrame("Ventana");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        inicializamos();
    }


    /**
     * Metodo que inicializa el default list model de persona y lo añade a la lista.
     */
    public void inicializamos(){
        dlmPersona=new DefaultListModel<Persona>();
        list1.setModel(dlmPersona);
    }
}
