package com.ignaciodeharo.clientesgym.gui;

import com.ignaciodeharo.clientesgym.base.Cliente;
import com.ignaciodeharo.clientesgym.base.Instructor;
import com.ignaciodeharo.clientesgym.base.Persona;
import com.ignaciodeharo.clientesgym.util.Util;
import org.xml.sax.SAXException;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Properties;

/**
 * Clase PersonaControlador que implemente ActionListener(Para poder actuar con los clicks), ListSelectionListener
 * (para poder ver en que parte de la lista clickamos) y WindowListener (para poder mostrar el mensaje de confirmación)
 */
public class PersonaControlador implements ActionListener, ListSelectionListener, WindowListener {
    private Ventana vista;
    private PersonaModelo modelo;
    private File ultimaRutaExportada;
    private String codigoComprobar;

    /**
     * Constructor de la clase que permite establecer los valores vista y mdoelo por defecto
     * @param vista la vista de la interfaz (Ventana)
     * @param modelo la instancia de la clase PersonaModelo
     */
    public PersonaControlador(Ventana vista, PersonaModelo modelo) {
        this.vista = vista;
        this.modelo = modelo;

        try {
            cargarDatosConfiguracion();
            cargarCodigo();
        } catch (IOException e) {
            e.printStackTrace();
        }

        addActionListener(this);
        addListSelectionListener(this);
        addWindowListener(this);

    }

    /**
     * Método qie permite controlar el nombre del objeto que seleccionas en la interfaz, para interactuar con él
     * @param e el objeto seleccionado
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String actionCommand = e.getActionCommand();

        switch (actionCommand) {
            case "Registrar":
                if (hayCamposVacios()) {
                    Util.msgError("Los siguientes campos no pueden estar vacios\n" +
                            "Nombre\nApellidos\nDNI\nFecha Alta\n" +
                            vista.txtLicenciaCuenta.getText());
                    break;
                }

                if (modelo.existeDNI(vista.txtDni.getText())) {
                    Util.msgError("Ya existe una persona con ese DNI\n+" +
                            vista.txtDni.getText());
                    break;
                }
                if (vista.clienteRadioButton.isSelected()) {
                    modelo.altaCliente(vista.txtNombre.getText(), vista.txtApellidos.getText(), vista.txtDni.getText(),
                            vista.datePicker.getDate(), Long.parseLong(vista.txtLicenciaCuenta.getText()));
                } else {
                    //System.out.println("Comprobar código --> " + this.codigoComprobar);
                    if (comprobarCodigo()){
                        modelo.altaInstructor(vista.txtNombre.getText(), vista.txtApellidos.getText(), vista.txtDni.getText(),
                                vista.datePicker.getDate(), vista.txtLicenciaCuenta.getText());
                    }else{
                        Util.msgError("Tienes que tener un código de verificación válido");
                        break;
                    }

                }
                limpiarCampos();
                refrescar();
                break;
            case "Importar":
                JFileChooser selectorFichero = Util.crearSelectorFicheros(ultimaRutaExportada, "Archivo XML", "xml");
                int opt = selectorFichero.showOpenDialog(null);
                if (opt == JFileChooser.APPROVE_OPTION) {
                    try {
                        modelo.importarXML(selectorFichero.getSelectedFile());
                    } catch (ParserConfigurationException ex) {
                        ex.printStackTrace();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    } catch (SAXException ex) {
                        ex.printStackTrace();
                    }
                    refrescar();
                }
                break;
            case "Exportar":
                JFileChooser selectorFichero2 = Util.crearSelectorFicheros(ultimaRutaExportada, "Archivos XML", "xml");
                int opt2 = selectorFichero2.showSaveDialog(null);
                if (opt2 == JFileChooser.APPROVE_OPTION) {
                    try {
                        modelo.exportarXML(selectorFichero2.getSelectedFile());
                        actualizarDatosConfiguracion(selectorFichero2.getSelectedFile());
                    } catch (ParserConfigurationException ex) {
                        ex.printStackTrace();
                    } catch (TransformerException ex) {
                        ex.printStackTrace();
                    }
                }
                break;
            case "Instructor":
                vista.txtLicenciaCod.setText("Licencia");
                break;
            case "Cliente":
                vista.txtLicenciaCod.setText("N Cuenta");
                break;
            case "Vaciar tabla":
                limpiarCampos();
                vista.dlmPersona.clear();
                vista.list1.clearSelection();
                modelo.obtenerPersonass().clear();
                break;

        }

    }

    /**
     * Permite comprobar si hay campos vacíos en la interfaz
     * @return boolean si hay o no campos vacíos
     */
    private boolean hayCamposVacios() {
        if (vista.txtNombre.getText().isEmpty() ||
                vista.txtApellidos.getText().isEmpty() ||
                vista.txtDni.getText().isEmpty() ||
                vista.txtLicenciaCuenta.getText().isEmpty() ||
                vista.datePicker.getText().isEmpty() ||
                vista.txtLicenciaCuenta.getText().isEmpty()){
            return true;
        }
        return false;
    }
    /**
     * Comprueba si el código de licencia es correcto o no (para poder registrar el Instructor)
     * @return boolean si concuerda o no el código.
     */
    private boolean comprobarCodigo(){

        if(vista.txtLicenciaCuenta.getText().equals(this.codigoComprobar)){
            return true;
        }
        return false;
    }

    /**
     * Limpia todos los campos de la interfaz (sin incluir el jlist)
     */
    private void limpiarCampos() {
        vista.txtNombre.setText(null);
        vista.txtApellidos.setText(null);
        vista.txtDni.setText(null);
        vista.txtLicenciaCuenta.setText(null);
        vista.datePicker.setText(null);
        vista.txtDni.requestFocus();
    }

    /**
     * Permite volver a establecer los datos del jList, borrándolos del mismo previamente
     */
    private void refrescar() {
        vista.dlmPersona.clear();
        for (Persona personita : modelo.obtenerPersonass()) {
            vista.dlmPersona.addElement(personita);
        }
    }

    /**
     * Permite el registro de las acciones de click sobre la interfaz
     * @param listener el objeto seleccionado/clickado
     */
    private void addActionListener(ActionListener listener) {
        vista.clienteRadioButton.addActionListener(listener);
        vista.instructorRadioButton.addActionListener(listener);
        vista.exportarButton.addActionListener(listener);
        vista.importarButton.addActionListener(listener);
        vista.registrarButton.addActionListener(listener);
        vista.vaciarTablaButton.addActionListener(listener);
    }

    /**
     * Permite controlar las acciones de ventana
     * @param listener las acciones de ventana
     */
    private void addWindowListener(WindowListener listener) {
        vista.frame.addWindowListener(listener);
    }

    /**
     * Permite controlar las selecciones del JList
     * @param listener la seleccion de la lista
     */
    private void addListSelectionListener(ListSelectionListener listener) {
        vista.list1.addListSelectionListener(listener);
    }

    /**
     * Carga los datos de configuración de ultima ruta exportada
     * @throws IOException excepción
     */
    private void cargarDatosConfiguracion() throws IOException {
        Properties configuracion = new Properties();
        configuracion.load(new FileReader("personas.conf"));
        ultimaRutaExportada = new File(configuracion.getProperty("ultimaRutaExportada"));
    }

    /**
     * Carga los datos de configuración de la contraseña/código de verificación
     * @throws IOException excepción dada.
     */
    private void cargarCodigo() throws IOException {
        Properties configuracion = new Properties();
        configuracion.load(new FileReader("personas.conf"));
        codigoComprobar = configuracion.getProperty("contrasenya");
        System.out.println(codigoComprobar);
    }

    /**
     * Actualiza la ultima ruta exportada
     * @param ultimaRutaExportada La ultima ruta exportada del documento
     */
    private void actualizarDatosConfiguracion(File ultimaRutaExportada) {
        this.ultimaRutaExportada = ultimaRutaExportada;
    }

    /**
     * Permite guardar la configuración del documeento (contraseña y ultima ruta exportada)
     * @throws IOException excepción dada
     */
    private void guardarConfiguracion() throws IOException {
        Properties configuracion = new Properties();
        configuracion.setProperty("ultimaRutaExportada", ultimaRutaExportada.getAbsolutePath());
        configuracion.setProperty("contrasenya", this.codigoComprobar);
        configuracion.store(new PrintWriter("personas.conf"), "Datos configuracion personas y codigo");

    }

    /**
     * Permite comprobar si el valor cambia (al hacer click sobre el jlist)
     * @param e La lista que estamos comprobando anteriormente
     */
    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting()) {
            Persona personaSlec = (Persona) vista.list1.getSelectedValue();
            vista.txtDni.setText((personaSlec.getDni()));
            vista.txtNombre.setText(personaSlec.getNombre());
            vista.txtApellidos.setText(personaSlec.getApellidos());
            vista.datePicker.setDate(personaSlec.getFechaAlta());

            if (personaSlec instanceof Cliente) {
                vista.clienteRadioButton.doClick();
                vista.txtLicenciaCuenta.setText(String.valueOf(((Cliente)personaSlec).getnCuenta()));
            } else {
                vista.instructorRadioButton.doClick();
                vista.txtLicenciaCuenta.setText(String.valueOf(((Instructor) personaSlec).getCodigoActivacion()));
            }
        }
    }

    /**
     * Permite comprobar mediante un mensaje de verificación, si de verdad queremos salir de la aplicación.
     * @param e
     */
    @Override
    public void windowClosing(WindowEvent e) {

        int resp = Util.msgConfirmacion("¿Desea salir de la ventana?", "Salir");
        if (resp == JOptionPane.OK_OPTION) {
            try {
                guardarConfiguracion();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            System.exit(0);
        }else{
            System.out.println("eeeea");
        }

    }


    @Override
    public void windowOpened(WindowEvent e) {
        
    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }
}
