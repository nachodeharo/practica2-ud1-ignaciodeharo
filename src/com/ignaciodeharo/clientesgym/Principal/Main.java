package com.ignaciodeharo.clientesgym.Principal;

import com.ignaciodeharo.clientesgym.gui.PersonaControlador;
import com.ignaciodeharo.clientesgym.gui.PersonaModelo;
import com.ignaciodeharo.clientesgym.gui.Ventana;
/**
 * Trabajo de exportación e importación de XMLs, con respecto a la temática individual, añadiendo el sistema MVC.
 * Mi aportación personal es:
 * - Añadido botón para vaciar todos los campos (incluido el jlist).
 * - Añadido el tipo de propiedad "contrasenya" donde solo los instructores con el código correcto podrán registrarse.
 * @author Nacho De Haro / SrWolfMoon
 * @see base,gui,Principal,util
 * @see gui.PersonaControlador
 * @version 24/11/2020   1.0
 * @since 1.8
 *
 */
public class Main {
    /**
     * Clase Main que ejecuta el programa, llama a la vista, al modelo y lo añade al controlador.
     * @param args
     */
    public static void main(String[] args) {
        Ventana vista = new Ventana();
        PersonaModelo modelo = new PersonaModelo();
        PersonaControlador controlador = new PersonaControlador(vista, modelo);
    }
}
